const port = 4004;

const bodyParser = require('body-parser');
const express = require('express');
const server = express();
const allowCors = require('./cors');
const queryParser = require('express-query-int');

// Configurando o body-parser
server.use(bodyParser.urlencoded({ extended: true }));
server.use(bodyParser.json());
server.use(allowCors);
server.use(queryParser());

server.get("/", function(req, res, next) {
    res.send("Backend rodando")
})

server.listen(port, function() {
    console.log(`BACKEND is running on port ${port}.`);
});
  
module.exports = server;
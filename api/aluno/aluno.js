const restful = require('node-restful');
const mongoose = restful.mongoose;

const alunoSchema = new mongoose.Schema({
    matricula: { type: Number, require: true},
    nome: {type: String, require: true}
});

module.exports = restful.model('Aluno', alunoSchema);
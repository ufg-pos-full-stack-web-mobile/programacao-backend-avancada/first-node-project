# Primeiro projeto Node Js


## Criando projeto:

O projeto foi criado com o comando 'npm init'
```
$ npm init
```

Ele vai mostrar as opções necessárias para criar o projeto.


### Adicionando as dependências do projeto:
Para adicionar as dependências do projeto, vamos usar o comando 'npm install', finalizando com o comando '--save' para adicionar as dependências no arquivo package.json
```
$ npm install express body-parser mongoose node-restful mongoose-paginate lodash express-query-int pm2 --save
```

### Adicionando nodemon para desenvolvimento:
Para adicionar a dependência do 'nodemon' em ambiente de desenvolvimento vamos utilizar o comando 'npm install' finalizando com o comando '--save-dev' para salvar como dependência de desenvolvimento
```
npm i nodemon --save-dev
```

### Adicionando inicializadores de ambiente de produção e desenvolvimento
Adicionamos o seguinte script no package.json para iniciar o projeto como desenvolvimento e produção, de acordo com a necessidade:
```
...
"scripts": {
  "dev": "nodemon",
  "production": "pm2 start loader.js --name backend"
},
...
```

